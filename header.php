<?php
/**
 * The Header for our theme.
 *
 * @package collisiongame
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
			
	<?php
 	wp_head();
	?>



	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-68784633-2"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());
	  gtag('config', 'UA-68784633-2', { 'anonymize_ip': true });
	  gtag('config', 'UA-68784633-2');
	</script>


</head>

<body <?php body_class(); ?>>

<div id="page" class="site">

	<header id="masthead" class="site-header">

		<div id="masthead-site-branding" class="site-branding">

			<div id="header-logo" class="site-title">
				<a id="header-logo-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
					<?php
					do_action('show_site_logo');
					?>
				</a>
			</div>

			<div id="header-sidebar">
				<?php dynamic_sidebar( 'header' ); ?>
			</div>

				
			<div id="responsive-menu-button-wrapper">
				<?php
				// Mobile menu
				if (shortcode_exists('responsive_menu_pro')) {
					echo do_shortcode('[responsive_menu_pro menu="Hoofdmenu"]');
				}
				?>
			</div>
		

		</div>

			
		<?php
		// Main site header/banner on homepage
		if (is_front_page()) 
		{
		?>
			<div id="site-header">
				<?php
				do_action('show_site_header');
				?>
			</div>
		<?php
		}
		?>


		<nav id="site-navigation">
		<?php
			wp_nav_menu( [
			    'menu' => 'main-menu',
			    'container_id' => 'main-menu-wrapper',
			    'container_class' => 'main-menu-wrapper',
			    'menu_id' => 'main-menu',
				'menu_class' => 'main-menu'
				]
			);
		?>
		</nav>

	</header>
