jQuery(document).ready(function ($) {


	// Main desktop menu
    $('#site-navigation ul li').hover(function () {
		$(this).children('ul').stop(true, false, true).slideToggle(300)
    })
    

    // Toggle between 360/Zoom
    $('.magic-360-wrapper').hide()
    $('.MagicToolboxSelectorsContainer img').bind("click touchstart", function() {
        toggleZoom(this, true)
    });
    $('#toggle360').bind("click touchstart", function() {
        toggleZoom(this, false)
    });

    function toggleZoom(id, openZoom) {
        
        if (openZoom) {
            // Open Zoom
            $('.mz-figure').show()
            $('.magic-360-wrapper').hide()
            $(id).addClass('mz-thumb-selected')
            $('#toggle360').removeClass('mz-thumb-selected')
        } else {
            // Open 360 View
            $('.mz-figure').hide()
            $('.magic-360-wrapper').show()
            $('.mz-thumb').removeClass('mz-thumb-selected')
            $('#toggle360').addClass('mz-thumb-selected')
        }
    }

    // Fancybox
    $('.gallery-item dt a').attr('data-fancybox', 'gallery')


    // Swiper Gallery homepage
    var mySwiper = new Swiper('.widget_media_gallery', {
        speed: 400,
        slidesPerView: 1,
    })
    


})

