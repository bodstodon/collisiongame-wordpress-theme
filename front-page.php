<?php
/**
 * The template for displaying all pages.
 * 
 * @package collisiongame
 */

get_header();

get_template_part( 'template-parts/main-container' );
?>

	<?php while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'template-parts/content-front-page' ); ?>
		
	<?php endwhile; ?>

<?php
get_sidebar();
get_footer();

