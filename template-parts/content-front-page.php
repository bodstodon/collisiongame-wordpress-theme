<?php
/**
 * Template part for the homepage
 *
 * @package collisiongame
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


	<div class="entry-content">
		<?php
		the_content();
		?>

		<footer class="entry-footer">
			<?php
			edit_post_link('Bewerken', '<span class="edit-link">', '</span>');
			?>
		</footer>
		
	</div>

</article>