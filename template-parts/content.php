<?php
/**
 * Template part for content
 *
 * @package collisiongame
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<?php
		if (is_singular()) {
			the_title('<h1 class="entry-title">', '</h1>');
		} else {
			the_title('<h2 class="entry-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h2>');
		}
		?>

	</header>

	<div class="entry-content">
		<?php
		the_content();
		?>

		<footer class="entry-footer">
			<?php
			edit_post_link('Bewerken', '<span class="edit-link">', '</span>');
			?>
		</footer>

		<?php
		wp_link_pages([
			'before' => '<div class="page-links">Pages:',
			'after'  => '</div>',
		]);
		?>
	</div>


</article>