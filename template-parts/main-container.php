<?php
/**
 * Template part to start content.
 *
 * @package collisiongame
 *
 **/
?>


<div id="main-wrapper">
	
	<main id="content" class="site-content">

		<?php
		if ( function_exists('yoast_breadcrumb') and !is_front_page()) {

			$breadcrumbs = yoast_breadcrumb('
				<div id="breadcrumbs-wrapper">
				<div class="sideleft"></div>
				<div class="middle">
				<p id="breadcrumbs">','</p>
				</div>
				<div class="sideright"></div>
				</div>', false);

			echo $breadcrumbs;
			}
		?>

		<?php
		//echo do_shortcode('[breadcrumb_back_button]');
		?>
	