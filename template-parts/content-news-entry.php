<?php
/**
 * Template part for blog items
 *
 * @package collisiongame
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<?php
			the_title('<h2 class="entry-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h2>');
		?>
		<span class="date-published">
			<?php echo esc_html( get_the_date() ); ?>
		</span>

	</header>

	<div class="entry-content">
		<?php
		// Show featured image?
		if (has_post_thumbnail()) { ?>
			<div class="featured">
				<?php the_post_thumbnail($size = 'medium'); ?>
			</div>
		<?php
		}
		
		the_content( __( 'Read more', 'collisiongame' ) );
		?>

		<footer class="entry-footer">
			<?php
			edit_post_link('Bewerken', '<span class="edit-link">', '</span>');
			?>
		</footer>

	</div>


</article>