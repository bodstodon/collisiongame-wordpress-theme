<?php
/**
* The Template for displaying all single posts.
*
* @package collisiongame
*/

get_header();

get_template_part( 'template-parts/main-container' );
?>

	<?php if ( have_posts() ) : ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'template-parts/content', 'single' ); ?>

	<?php endwhile; ?>

	<?php else : ?>

<?php get_template_part( 'template-parts/no-results', 'index' ); ?>

<?php endif; ?>

<?php
get_sidebar();
get_footer();

