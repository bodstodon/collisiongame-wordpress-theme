<?php
/**
 * Collision the Game Wordpress Theme
 */

define('COLLISIONGAME_THEME_VERSION', '1.0.0');

define('DISALLOW_FILE_EDIT', true );
//error_reporting(0);


// Require once the Composer Autoload
if (file_exists(dirname(__FILE__) . '/vendor/autoload.php')) {
    require_once dirname(__FILE__) . '/vendor/autoload.php';
}


// Initialize all core classes and pass plugin settings
if (class_exists('CollisionGame\\Init')) {
    // Init plugin settings
    require_once dirname(__FILE__) . '/config.php';
    CollisionGame\Init::run();
}

