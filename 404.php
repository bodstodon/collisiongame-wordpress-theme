<?php
/**
 * The template for displaying a 404
 *
 * @package collisiongame
 */

get_header();

get_template_part('template-parts/main-container');
?>

<article id="post-<?php the_ID();?>" <?php post_class();?>>

	<header class="entry-header">
		<h1 class="entry-title"><?php _e('Page Not Found', 'collisiongame');?></h1>
	</header>

	<div class="entry-content">

		<p>
		<?php
		 _e('The page you were looking for could not be found.', 'collisiongame');
		?>
		</p>

		<?php
		get_search_form([
        	'label' => __('404 not found', 'collisiongame'),
		]);
		?>

	</div>

</article>

<?php
get_sidebar();
get_footer();
