const gulp = require('gulp');
const sass = require('gulp-sass');
const cached = require('gulp-cached');
const sassPartialsImported = require('gulp-sass-partials-imported');
const browserSync = require('browser-sync').create();
const autoprefixer = require('gulp-autoprefixer');


// Settings
let scss_dir = 'scss/';
let output_dir = 'assets/';
let node_paths = ['node_modules/breakpoint-sass/stylesheets'];
let proxy_server = 'http://www.collisionthegame.newlocal.com';


// Sass to css
gulp.task('browsersync', function() {

    return gulp.src(['scss/*.scss'])
    .pipe(sassPartialsImported(scss_dir, node_paths))
    .pipe(sass(
            {
            //outputStyle: 'extended',
            outputStyle: 'compressed',
            includePaths: node_paths,
            }
        ).on('error', function(err) {
        console.error(err.message);
        browserSync.notify(err.message, 3000);
        this.emit('end');
    }))
    .pipe(autoprefixer({
        browsers: ['last 2 versions'],
        cascade: false
    }))
    .pipe(gulp.dest(output_dir + 'css'))
    .pipe(browserSync.stream());
});


// Watch stuff
gulp.task('watch', ['browsersync'], function() {
    browserSync.init({
           proxy: proxy_server
    });
    gulp.watch('scss/**/*.scss', ['browsersync']);
    //gulp.watch('assets/*.js').on('change', browserSync.reload);
    //gulp.watch('gulpfile.js').on('change', () => process.exit(0));
});


// run tasks
gulp.task('default', ['watch']);
