<?php
/**
 * @package collisiongame
 */

use \CollisionGame\Base\Config;

$collison_game_config = [
    'plugin_name'    => 'collisiongame',
    'plugin_version' => COLLISIONGAME_THEME_VERSION,
    'plugin_path'    => plugin_dir_path(__FILE__),
    'plugin_url'     => get_template_directory_uri(),
    'number_of_sidebars_footer' => 2
];

Config::set($collison_game_config);
