<?php
/**
 * Search results
 *
 * @package collisiongame
 */

get_header();

get_template_part( 'template-parts/main-container' );
?>

	<header>
		<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'collisiongame' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
	</header>

	<?php if ( have_posts() ) : ?>


		<div class="entry-content">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php
			$post_type = get_post_type();
			get_template_part( 'template-parts/content');
			?>

		<?php
		endwhile; 
		?>

		<div class="pagination">
			<?php
				echo paginate_links([
					'current' => max(1, get_query_var('paged')),
					'end_size'     => 1,
					'mid_size'     => 1,
					'prev_text' => sprintf('%1$s', __('Previous', 'collisiongame')),
					'next_text' => sprintf('%1$s', __('Next', 'collisiongame'))
				]);
			?>
		</div>


	<?php else : ?>

		<p>
			<?php
			_e('Sorry. Nothing found.', 'collisiongame');
			?>
		</p>

	<?php endif; ?>

<?php
get_sidebar();
get_footer();