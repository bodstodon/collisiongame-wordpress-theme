<?php
/**
 * The template for displaying all pages.
 * 
 * @package collisiongame
 */

get_header();

get_template_part( 'template-parts/main-container' );
?>


  <div class="entry-content">

  	<?php
	if (have_posts()) {
    ?>
		<header>
			<h1 class="page-title">
				<?php
				echo get_queried_object()->name;
    			?>
			</h1>
		</header>

		<div class="entry-content">
			<?php

			$media_category_id = get_queried_object()->term_id;
	
			do_action('show_link_to_gallery_or_content', $media_category_id);
			?>
		</div>

		<?php
	} // end posts found

	// Simple Share Buttons Adder
	if (shortcode_exists('ssba')) {
		echo do_shortcode('[ssba]');
	}
	?>
	</div>

<?php
get_sidebar();
get_footer();
