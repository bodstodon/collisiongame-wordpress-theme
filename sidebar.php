<?php
/**
 * The sidebar containing the main widget area
 *
 * @package collisiongame
 */

if (is_front_page()) { ?>

	<div id="sidebar">

		<?php 
		dynamic_sidebar( 'sidebar' );
		?>

	</div>

<?php } ?>
