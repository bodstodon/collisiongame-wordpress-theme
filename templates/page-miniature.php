<?php
/**
 * Template name: Miniature
 *
 * @package collisiongame
 */

get_header();

get_template_part('template-parts/main-container');
?>

<?php if ( have_posts() ) : ?>

	

<?php while ( have_posts() ) : the_post(); ?>

	<article id="post-<?php the_ID();?>" <?php post_class();?>>

		<header>
			<?php the_title('<h1 class="entry-title">', '</h1>');?>
		</header>

		<div class="entry-content zoom">

		<?php
		$slug = $post->post_name;

		// Magic360 plugin
		if (function_exists('magictoolbox_WordPress_Magic360_get_data')) {
			
			$m_tool = magictoolbox_WordPress_Magic360_get_data("shortcode", '"' . $slug . '"');
			// $m_tool[0]->images; // string: 1098,1099,1100,1101...
			$images = explode(',', $m_tool[0]->images);
			$first_image_id = $images[0];
			$image_meta = wp_get_attachment_image_src($first_image_id, 'thumbnail', false);
			?>
			<div class="magic-360-wrapper">
				<?php
				echo do_shortcode('[magic360 id="' . $slug . '"]');
				?>
			</div>
			<?php
			echo do_shortcode('[magiczoomplus id="' . $slug . '"]');
			?>
			<div id="toggle360">
				<img src="<?php echo $image_meta[0]; ?>" alt="360"><span class="label">360</span>
			</div>
			<?php
		}
		?>

	</div>
	<div class="content entry-content">
		<?php the_content();?>
	</div>

	</article>
	
	<?php endwhile; ?>
	
	<?php else : ?>
<?php get_template_part( 'template-parts/no-results', 'index' ); ?>

<?php endif; ?>


<?php
get_sidebar();
get_footer();
