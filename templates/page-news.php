<?php
/**
 * The template for displaying all pages.
 *
 * Template name: News
 *
 * @package collisiongame
 */

get_header();

get_template_part('template-parts/main-container');
?>
<?php if ( have_posts() ) : ?>
	
<?php while ( have_posts() ) : the_post(); ?>

	<article id="post-<?php the_ID();?>" <?php post_class();?>>

	<header>
		<h1 class="page-title"><?php esc_html_e('News', 'collisiongame');?></h1>
	</header>

	<div class="entry-content">

		<?php
		// Show only posts with "news" category
		$news = new WP_Query(['category_name' => 'news', 'paged' => $paged]);

		if (!empty($news->posts)) {

    		if ($news->have_posts()) {

        		while ($news->have_posts()): $news->the_post();
		            get_template_part('template-parts/content-news-entry');
		        endwhile;
			}
			?>
			<div class="pagination">
				<?php
					echo paginate_links([
						'total' => $news->max_num_pages,
						'current' => max(1, get_query_var('paged')),
						'end_size'     => 1,
            			'mid_size'     => 1,
						'prev_text' => sprintf('%1$s', __('Previous', 'collisiongame')),
						'next_text' => sprintf('%1$s', __('Next', 'collisiongame'))
					]);
				?>
			</div>
		<?php
		}
		?>

	</div>

	</article>
	
	<?php endwhile; ?>
	
	<?php else : ?>

<?php get_template_part( 'template-parts/no-results', 'index' ); ?>

<?php endif; ?>
<?php
get_sidebar();
get_footer();
