<?php
/**
 * @package collisiongame
 */
namespace CollisionGame;

/**
 * Main plugin class
 */
final class Init
{


    // Register en init alle classes
    public static function run()
    {
        self::register_classes();
    }


    // Lijst van alle te registeren classes
    public static function get_classes()
    {
        return [
            Admin\MenuBar::class,
            Admin\Customizer::class,
            Base\Config::class,
            Base\Enqueue::class,
            Base\Shortcode::class,
            Setup\Image::class,
            Setup\Header::class,
            Setup\Sidebar::class,
            Setup\Title::class,
            ImageGallery\ImageGallery::class,
            Language\Language::class,
            Plugins\FancyBoxScopial::class,
            Plugins\Swiper::class,
			Plugins\Yoast::class,
        ];
    }

    // Register class and initiate init method
    public static function register_classes()
    {
        foreach (self::get_classes() as $class) {
            $service = new $class();
            if (method_exists($service, 'init')) {
                $service->init();
            }
        }
    }


}
