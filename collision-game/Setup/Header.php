<?php
/**
 * @package collisiongame
 */

namespace CollisionGame\Setup;


class Header
{

    /**
     * Init class and all actions/filters
     */
    public function init()
    {

        add_action('show_site_logo', [$this, 'show_site_logo']);

        add_action('show_site_header', [$this, 'show_site_header']);

        add_filter('body_class', [$this, 'add_to_bodyclass']);

    }


    /**
     * Show default logo or logo from customizer
     *
     * @return void
     */
    public function show_site_logo()
    {

        $logo_id = the_custom_logo();
        $logo = wp_get_attachment_image_src($logo_id, 'full');

        if (has_custom_logo()) {
            ?>
				<img src="<?php echo esc_url($logo[0]); ?>" alt="Collision Logo" />
		    <?php
		    } else {
            ?>
				<img src="<?php echo esc_url(get_template_directory_uri() . '/assets/images/collision-logo.png'); ?>" alt="Collision Logo" />
		<?php
        }
    }


    /**
     * Show default site header or from customizer
     *
     * @return void
     */
    public function show_site_header()
    {

        if (has_custom_header()) {
            ?>
			<img src="<?php header_image();?>" alt="Collision Header" />
		<?php
            } else {
        ?>
			<img src="<?php echo esc_url(get_template_directory_uri() . '/assets/images/header.jpg'); ?>" alt="Collision Header" />
		<?php
        }
    }


    /**
     * Add slug and category to the body class
     * @param  array $classes
     * @return array
     */
    public function add_to_bodyclass($classes)
    {

        global $post;

        if (isset($post)) {
            $classes[] = $post->post_type . '-' . $post->post_name;
        }
        return $classes;
    }

}
