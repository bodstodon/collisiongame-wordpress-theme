<?php
/**
 * @package collisiongame
 */

namespace CollisionGame\Setup;

class Title
{

    /**
     * Init class and all actions/filters
     */
    public function init()
    {
        // Let WordPress manage the document title.
        add_theme_support('title-tag');

    }

}
