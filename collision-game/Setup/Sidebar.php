<?php
/**
 * @package collisiongame
 */

namespace CollisionGame\Setup;

use CollisionGame\Base\Config;


class Sidebar
{

	private $number_of_sidebars;


    /**
     * Init class and all actions/filters
     */
    public function init()
    {

        $this->number_of_sidebars_footer();

		add_action('widgets_init', [$this, 'register_sidebars']);
				
		// Enable shortcode support in widgets
		add_filter('widget_text', 'do_shortcode');

    }

    /**
     * Get the number of sidebars from the config file
     *
     * @return int
     */
    private function number_of_sidebars_footer()
    {
        $this->number_of_sidebars_footer = Config::get('number_of_sidebars_footer');
    }



    /**
     * Register als sidebars
     *
     * @return void
     */
    public function register_sidebars()
    {

        // Real Sidebar
        register_sidebar([
            'name' => __('Sidebar', 'collisiongame'),
            'id' => 'sidebar',
            'before_widget' => '<aside id="%1$s" class="widget-wrapper %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ]);

        // Header
        register_sidebar([
            'name' => __('Header', 'collisiongame'),
            'id' => 'header',
            'before_widget' => '<aside id="%1$s" class="widget-wrapper %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ]);

        // Footer
        for ($nr = 1; $nr <= $this->number_of_sidebars_footer; $nr++) {

            register_sidebar([
                'name' => __('Footer' . $nr, 'collisiongame'),
                'id' => 'footer-sidebar-' . $nr,
                'before_widget' => '<div id="%1$s" class="widget-wrapper %2$s">',
                'after_widget' => '</div>',
                'before_title' => '<h3 class="widget-title">',
                'after_title' => '</h3>',
            ]);
        }
    }

    
}
