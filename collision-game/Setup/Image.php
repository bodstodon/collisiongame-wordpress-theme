<?php
/**
 * @package collisiongame
 */

namespace CollisionGame\Setup;


class Image
{

    /**
     * Init class and all actions/filters
     */
    public function init()
    {

        add_action('after_setup_theme', [$this, 'add_image_sizes']);

        add_filter('image_size_names_choose', [$this, 'register_custom_sizes']);

        add_filter('the_content', [$this,'use_figure_tag']);
   
    }


    /**

     * Add extra image sizes:
     *
     * Thumbnail = 150x150 (cropped)
     * Medium = 250x250
     * Medium_square = 250x250 (cropped)
     * Medium_middle = 400x400
     * Medium_middle_square = 400x400 (cropped)
     * Medium_large = 600x600
     * Large = 1024x1024
     * Full = ???x???
     */
    public function add_image_sizes()
    {

        add_image_size('medium', '250', '250', false);
        add_image_size('medium_square', '250', '250', true);
        add_image_size('medium_middle', '400', '400', false);
        add_image_size('medium_middle_square', '400', '400', true);
        add_image_size('medium_large', '600', '600', false);
        
        // Link images to file, not attachment page
        update_option('image_default_link_type', 'file');

    }

    
    /**
     * Register extra image names for media library
     * @param  array $sizes
     * @return array $sizes
     */
    public function register_custom_sizes($sizes)
    {
        return array_merge($sizes, [
            'medium_square' => __('Medium Square'),
            'medium_middle' => __('Medium Middle'),
            'medium_middle_square' => __('Medium Middle Square'),
            'medium_large' => __('Medium Large'),

        ]);
    }



    /**
     * Add an extra <figure> container tag to all wordpress images
     * 
     * @return string
     */
    public function use_figure_tag($content) {

        $pattern = '/(<img[^>]*class=\"([^>]*?)\"[^>]*>)/i';
        $replacement = '<figure class="image-wrapper $2"><div class="image">$1</div></figure>';
        $content = preg_replace($pattern, $replacement, $content);
    
        return $content;
    }
   
 

}
