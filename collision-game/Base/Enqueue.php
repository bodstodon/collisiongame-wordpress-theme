<?php
/**
 * @package collisiongame
 */

namespace CollisionGame\Base;

use CollisionGame\Base\Config;


class Enqueue
{

    /**
     * Init class and all actions/filters
     */
    public function init()
    {
        add_action('wp_enqueue_scripts', [$this, 'enqueue_styles']);
        add_action('wp_enqueue_scripts', [$this, 'enqueue_scripts']);
    }

    /**
     * Site CSS
     */
    public function enqueue_styles()
    {

        wp_register_style('lato', 'http://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext', [], COLLISIONGAME_THEME_VERSION);
        wp_enqueue_style('lato');

        wp_register_style('normalize', Config::get('plugin_url') . '/assets/css/normalize.css', null, Config::get('plugin_version'));
        wp_enqueue_style('normalize');

        wp_register_style(Config::get('plugin_name') . '-style', Config::get('plugin_url') . '/assets/css/style.css', null, Config::get('plugin_version'));
        wp_enqueue_style(Config::get('plugin_name') . '-style');

        wp_enqueue_style('dashicons');

    }

    /**
     * Site Javascript
     */
    public function enqueue_scripts()
    {

        wp_register_script(Config::get('plugin_name'), Config::get('plugin_url') . '/assets/js/collisiongame.js', null, Config::get('plugin_version'));
        wp_enqueue_script(Config::get('plugin_name'));

    }

}
