<?php
/**
 * @package collisiongame
 */

namespace CollisionGame\Base;


class Shortcode
{


    /**
     * Catch all boolean values for shortcode arguments
     *
     * @param mixed $value
     * @return boolean
     */
	public function force_true_boolean($value)
    {
        if ($value === 'false' or $value === false or $value === 0) {
            return false;
        }

        if ($value === 'true' or $value === true or $value === 1) {
            return true;
        }
    }


    /**
     * Convert an array of ids to a comma seperated string
     *
     * @param array|string $ids
     * @return string
     */
    public function convert_array_of_ids_to_string($ids)
    {

        if (is_array($ids)) {
            return implode(',', $ids);
        } else {
            if (is_string($ids)) {
                return $ids;
            }
        }

    }
}
