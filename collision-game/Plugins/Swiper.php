<?php
/**
 * @package collisiongame
 */

namespace CollisionGame\Plugins;

use CollisionGame\Base\Config;


class Swiper
{

    /**
     * Init class and all actions/filters
     */
    public function init()
    {
        add_action('wp_enqueue_scripts', [$this, 'enqueue_styles']);
        add_action('wp_enqueue_scripts', [$this, 'enqueue_scripts']);
    }


    /**
     * Site CSS
     */
    public function enqueue_styles()
    {

        wp_register_style('swiper', Config::get('plugin_url') . '/assets/swiper/swiper.min.css', null, Config::get('plugin_version'));
        wp_enqueue_style('swiper');

    }

    
    /**
     * Site Javascript
     */
    public function enqueue_scripts()
    {

        wp_register_script('swiper', Config::get('plugin_url') . '/assets/swiper/swiper.min.js', ['jquery'], Config::get('plugin_version'));
        wp_enqueue_script('swiper');

    }

}
