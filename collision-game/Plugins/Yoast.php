<?php
/**
 * @package collisiongame
 */

namespace CollisionGame\Plugins;


class Yoast
{

    /**
     * Init class and all actions/filters
     */
    public function init()
    {

        add_filter('wpseo_breadcrumb_links', [$this, 'breadcrumb_links']);

        add_shortcode('breadcrumb_back_button', [$this, 'breadcrumb_back_button']);

    }

	/**
	 * Yoast Breadcrumbs adjustments
	 * @param  array $links
	 * @return array
	 */
    public function breadcrumb_links($links)
    {
        return $links;
    }

    /**
     * Back button to the parent breadcrumb
     *
     * @return void
     */
    public function breadcrumb_back_button()
    {

        if (function_exists('yoast_breadcrumb')) {

            // Get the breadcrumbs from Yoast
            $breadcrumbs = yoast_breadcrumb('', '', false);
            preg_match_all('/<a href="(.*?)<\/a>/s', $breadcrumbs, $matches);
            $last = count($matches[1]) - 1;

            $backbutton = '<div id="back-button-wrapper">';

            if ($last > 0 and $matches[1][$last] != '') {
                // Link to the parent
                $backbutton .= '<a class="button" href="' . $matches[1][$last] . '</a>';
            } else {
                // Link to homepage
                $backbutton .= '<a class="button" href="'. get_home_url().'">Home</a>';
            }

            $backbutton .= '</div>';

            return $backbutton;
        }
    }

    
}
