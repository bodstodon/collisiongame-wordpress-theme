<?php
/**
 * @package collisiongame
 */

namespace CollisionGame\Plugins;

class TheEventCalendar
{

    /**
     * Init class and all actions/filters
     */
    public function init()
    {

		add_action( 'pre_get_posts', [$this,'tribe_post_date_ordering'], 50 );

	}



	/**
	 * Order events calendar posts by date
	 * @param  string	$query	Query from events calender
	 * @return void
	 */
	public function tribe_post_date_ordering( $query )
	{
		if ( !empty( $query->tribe_is_multi_posttype ) )
		{
			remove_filter( 'posts_fields', ['Tribe__Events__Query', 'multi_type_posts_fields' ) ];
			$query->set( 'order', 'DESC' );
		}
	}


}