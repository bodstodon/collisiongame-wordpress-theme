<?php
/**
 * @package collisiongame
 */

namespace CollisionGame\Admin;


use CollisionGame\Base\Config;

class Customizer
{


    /**
     * Init class and all actions/filters
     */
    public function init()
    {
		add_action('after_setup_theme', [$this, 'custom_logo']);
		add_action('after_setup_theme', [$this, 'custom_header']);
	}
	


	/**
	 * Enable to change logo in theme customizer
	 *
	 * @return void
	 */
    public function custom_logo() {

		$args = [
		  'height' => 100,
		  'width' => 400,
		  'flex-height' => true,
		  'flex-width' => true,
		  'header-text' => ['site-title', 'site-description'],
		];
	
  		add_theme_support('custom-logo', $args);
	}


	/**
	 * Enable to change the site in theme customizer
	 *
	 * @return void
	 */
	public function custom_header() {
		$args = [
			'default-image'      => Config::get('plugin_url') . '/assets/images/site-header.jpg',
			'width'              => 1920,
			'height'             => 250,
			'flex-width'         => true,
			'flex-height'        => true,
		];
		add_theme_support( 'custom-header', $args );
	}

}
