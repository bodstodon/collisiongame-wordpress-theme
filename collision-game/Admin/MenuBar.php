<?php
/**
 * @package collisiongame
 */

namespace CollisionGame\Admin;


class MenuBar
{

    /**
     * Init class and all actions/filters
     */
    public function init()
    {

        add_action('admin_bar_menu', [$this, 'add_media_query_breakpoints'], 90);

    }

    /**
     * Show css breakpoints in admin bar
     * @param  object $admin_bar
     * @return object
     */
    public function add_media_query_breakpoints($admin_bar)
    {

        $admin_bar->add_menu([
            'id' => 'show-breakpoint-used',
            'title' => '
			   <div id="show-breakpoint-used">
				<span class="xxs">xxs</span>
				<span class="xs">xs</span>
				<span class="sm">sm</span>
				<span class="md">md</span>
				<span class="lg">lg</span>
				<span class="xl">xl</span>
			</div>',
        ]);

        return $admin_bar;
    }
}
