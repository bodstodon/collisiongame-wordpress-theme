<?php
/**
 * @package collisiongame
 */

namespace CollisionGame\ImageGallery;

use CollisionGame\Base\Shortcode;


class ImageGallery
{

    private $shortcode;

    /**
     * Init class and all actions/filters
     */
    public function init()
    {

        $this->shortcode = new Shortcode();

        add_filter('the_content', [$this, 'remove_br_gallery']);

        // Remove styling from default Wordpress gallery
        add_filter('use_default_gallery_style', '__return_false');

        add_action('show_link_to_gallery_or_content', [$this, 'show_link_to_gallery_or_content']);

        add_shortcode('show_link_to_gallery', [$this, 'show_link_to_gallery']);

    }

    /**
     * Remove default wordpress gallery inline breaks and styling
     * @param  string $output
     * @return string
     */
    public function remove_br_gallery($output)
    {
        $output = str_replace('<br style="clear: both" />', '', $output);
        return $output;
    }



    /**
     * Show the contents of a media category in a gallery
     * or show a button/link (of the parent) to the child media gallery
     *
     * @param int $media_category_id*
     * 
     * @return void
     */
    public function show_link_to_gallery_or_content($media_category_id)
    {

        $children_ids = get_term_children($media_category_id, 'media_category');
        
        // If the media category has children show a link/button
        if (isset($children_ids[0])) {

            // Put the newly generated media categories on top
            $children_ids = array_reverse($children_ids);
            $string_ids = $this->shortcode->convert_array_of_ids_to_string($children_ids);

            echo do_shortcode('[show_link_to_gallery id="' . $string_ids . '"]');

        } else {

            // Show a gallery
            $gallery = do_shortcode('[gallery media_category="' . $media_category_id . '" order="DESC" orderby="post_date" size="medium_square" link="file"]');
            echo $this->remove_br_gallery($gallery);
        }
    }

    /**
     * Show a link or button to a media category page
     */
    public function show_link_to_gallery($atts)
    {

        // Shortcode atts
        $args = wp_parse_args($atts,
            [
                'id' => [],
                'slug' => '',
                'showimage' => true,
            ]
        );

        $id = $args['id'];
        $slug = $args['slug'];
        $showimage = $this->shortcode->force_true_boolean($args['showimage']);

        $html = '';

        // Use slug (if given) otherwise ids
        if ($slug != '') {
            $ids[] = get_term_by('slug', $slug, 'media_category')->term_id;
        } else {
            $ids = explode(',', $id);
        }

        foreach ($ids as $id) {

            $term = get_term_by('id', $id, 'media_category');

            // Grab the first image from the media category
            $image_url = $this->get_gallery_image_url_by_id($term->slug);

            // Image or not?
            $has_image = ($showimage) ? 'has-image' : '';

            $html .= '<div class="media-category-wrapper ' . $has_image . '">';
            $html .= '<a href="/media-category/' . $term->slug . '" id="media-category-' . $id . '" class="media-category link">';
            $html .= '<h3 class="title">' . $term->name . '</h3>';
            $html .= '<div class="image-wrapper">';
            if ($image_url != '' and $showimage) {
                $html .= '<img src="' . $image_url . '" alt="' . $term->name . '" />';
            }
            $html .= '</div>';
            $html .= '</a>';
            $html .= '</div>';
        }

        return $html;

    }

    public function get_gallery_image_url_by_id($catslug, $size = 'medium_square')
    {

        $query = new \WP_Query(
            $args = [
                'post_type' => 'attachment',
                'posts_per_page' => -1,
                'post_status' => 'inherit',
                'post_parent' => null,
                'tax_query' => [
                    [
                        'taxonomy' => 'media_category',
                        'field' => 'slug',
                        'terms' => $catslug,
                    ],
                ],
            ]
        );

        // Get the first post
        if (isset($query->posts[0]->ID)) {
            return wp_get_attachment_image_src($query->posts[0]->ID, $size)[0];
        }

    }

}
