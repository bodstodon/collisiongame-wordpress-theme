<?php
/**
 * @package collisiongame
 */

namespace CollisionGame\Language;

use CollisionGame\Base\Config;

class Language
{

    /**
     * Init class and all actions/filters
     */
    public function init()
    {

        add_action('after_setup_theme', [$this, 'load_theme_textdomain']);

    }

    /**
     * Language/localization i18n
     */
    public function load_theme_textdomain()
    {
        load_theme_textdomain(Config::get('plugin_name'), Config::get('plugin_path') . '/languages');
    }

}
