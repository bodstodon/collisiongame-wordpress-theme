<?php
/**
 * @package collisiongame
 */

?>

</main>

</div>

<footer id="footer">

	<div id="footer-sidebar-wrapper">

		<?php
		// Loop footer sidebars
		$number_of_sidebars_footer = \CollisionGame\Base\Config::get('number_of_sidebars_footer');

		for ($t = 1; $t <= $number_of_sidebars_footer; $t++) {
			?>
			<div class="footer-sidebar" id="footer-sidebar-<?php echo $t; ?>">
				<?php
					// Check if exists
					if (is_active_sidebar('footer-sidebar-' . $t)) {
						dynamic_sidebar('footer-sidebar-' . $t);
					}
					?>
			</div>

		<?php } ?>

			
		<div id="copyright-wrapper" role="contentinfo">
			<span>Developed by: Bodstodon</span>
			<span class="copyright">Code Orange Games <?php echo "&copy; " . date("Y"); ?></span>
		</div>
	</div>



</footer>


<?php if (current_user_can('administrator')) { ?>
	<div id="theme-version">
		<ul>
			<li>Theme: <?php echo COLLISIONGAME_THEME_VERSION ?></li>
		</ul>
	</div>
<?php } ?>


</div> 

<?php
wp_footer();
?>

</body>

</html>